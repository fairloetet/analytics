# Installation

## Docker

docker-compose.yml ist eine einfache Version, basierend auf https://medium.com/towards-data-engineering/quick-setup-configure-superset-with-docker-a5cca3992b28

Dieses starten mit <code>docker compose -f "docker-compose.dev.yml" up -d --build </code>

docker-compose-non-dev.yml ist das Original aus https://github.com/apache/superset/tree/master, ...-dev.yml entsprechend das ...yml von dort.
Das erwartet aber ein clonen des ganzen Repositories, siehe Anleitung unter https://superset.apache.org/docs/installation/installing-superset-using-docker-compose/
Ich habe es ausprobiert, aber es benötigt zB das ganze Verzeichnis https://github.com/apache/superset/tree/master/superset-websocket

Zur Initialisierung nach Installation siehe superset-init.sh oder https://github.com/apache/superset/blob/master/docker/docker-init.sh

Die original-compose files sind von https://github.com/apache/superset/tree/master

## Nach erstem Start

In der Shell des Superset Dashboard Containers gibt es folgendes zu tun:
* Einmal muss der admin User angelegt werden: <code>superset fab create-admin --username "$ADMIN_USERNAME" --firstname Superset --lastname Admin --email "$ADMIN_EMAIL" --password "$ADMIN_PASSWORD"</code>
* Bei jeder neuen Version, insbesondere beim ersten Mal, müssen die Datenbankmigrationen laufen: <code>superset db upgrade</code>
* Am Anfang und bei jeder neuen Version müssen u.a. die Roles und Permissions angelegt werden: <code>superset init</code>


# Nutzung

## Öffnen

http://localhost:8088

Login mit den in bi.dev.env hinterlegten Anmeldedaten
