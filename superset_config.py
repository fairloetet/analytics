import os

FEATURE_FLAGS = {
    "ENABLE_TEMPLATE_PROCESSING": True,
    "EMBEDDABLE_CHARTS": True,
    "EMBEDDED_SUPERSET": True,
    "ALLOW_ADHOC_SUBQUERY": True,
    "DASHBOARD_RBAC": True,
    "DASHBOARD_VIRTUALIZATION": True,
    "DATAPANEL_CLOSED_BY_DEFAULT": True,
    "ENABLE_ADVANCED_DATA_TYPES": True,  # experimental
    "TAGGING_SYSTEM": True,  # experimental
    "ALERT_REPORTS": True,  # testing
    "HORIZONTAL_FILTER_BAR": True,   # testing
    "ALLOW_FULL_CSV_EXPORT": True,  # testing, may cause server performance issues
    "DASHBOARD_CROSS_FILTERS": True,  # deprecated
    "ENABLE_JAVASCRIPT_CONTROLS": True  # deprecated
}

ENABLE_PROXY_FIX = True
SECRET_KEY = os.getenv("SECRET_KEY", default="this_secret_is_uncovered")
PREVENT_UNSAFE_DB_CONNECTIONS = False

TALISMAN_ENABLED = False  # https://superset.apache.org/docs/security/

PUBLIC_ROLE_LIKE = "Gamma"
